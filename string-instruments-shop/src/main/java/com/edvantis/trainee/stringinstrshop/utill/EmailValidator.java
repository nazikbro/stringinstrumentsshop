/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.utill;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ����
 *
 */
public class EmailValidator {

	private static Pattern pattern;
	private static Matcher matcher;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private EmailValidator() {}
	
	static {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	public static boolean validate(final String text) {

		matcher = pattern.matcher(text);
		return matcher.matches();

	}
}
