/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.StringInstrumentDTO;

/**
 * @author Назік
 *
 */
public interface StringInstrumentDAO extends GenericDAO<StringInstrumentDTO, Integer> {
	public List<StringInstrumentDTO> findAll();

	public StringInstrumentDTO findByID(Integer id);
}
