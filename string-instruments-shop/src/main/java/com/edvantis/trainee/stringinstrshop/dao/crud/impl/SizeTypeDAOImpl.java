/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.SizeTypeDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.SizeTypeDTO;

/**
 * @author Назік
 *
 */
public class SizeTypeDAOImpl extends AbstractDAO<SizeTypeDTO, Integer> implements SizeTypeDAO {

	@SuppressWarnings("unchecked")
	public List<SizeTypeDTO> findAll() {
		return (List<SizeTypeDTO>) super.findAll(SizeTypeDTO.class);
	}

	public SizeTypeDTO findByID(Integer id) {
		return super.findByID(SizeTypeDTO.class, id);
	}

}
