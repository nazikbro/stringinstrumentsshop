/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.enums;

/**
 * @author Назік
 *
 */
public enum StringInstrumentDAOType {
	BODY_MATERIAL, BOW, INSTRUMENT_TYPE, OCCUPATION, ORDERS, PERSON, SIZY_TYPE, SOUND_TYPE, STRING_TYPE, STRING_INSTRUMENT

}
