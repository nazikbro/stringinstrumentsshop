/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.BodyMaterialDTO;

/**
 * @author Назік
 *
 */
public interface BodyMaterialDAO extends GenericDAO<BodyMaterialDTO, Integer> {

	public BodyMaterialDTO findByName(String name);
	
	public List<BodyMaterialDTO> findAll();

	public BodyMaterialDTO findByID(Integer id);
	
}
