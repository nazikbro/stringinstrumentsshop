/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.StringTypeDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.StringTypeDTO;

/**
 * @author Назік
 *
 */
public class StringTypeDAOImpl extends AbstractDAO<StringTypeDTO, Integer> implements StringTypeDAO {

	@SuppressWarnings("unchecked")
	public List<StringTypeDTO> findAll() {
		return (List<StringTypeDTO>) super.findAll(StringTypeDTO.class);
	}

	public StringTypeDTO findByID(Integer id) {
		return super.findByID(StringTypeDTO.class, id);
	}

}
