/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.OrdersDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.OrdersDTO;

/**
 * @author Назік
 *
 */
public class OrdersDAOImpl extends AbstractDAO<OrdersDTO, Integer> implements OrdersDAO {

	@SuppressWarnings("unchecked")
	public List<OrdersDTO> findAll() {
		return (List<OrdersDTO>) super.findAll(OrdersDTO.class);
	}

	public OrdersDTO findByID(Integer id) {
		return super.findByID(OrdersDTO.class, id);
	}

}
