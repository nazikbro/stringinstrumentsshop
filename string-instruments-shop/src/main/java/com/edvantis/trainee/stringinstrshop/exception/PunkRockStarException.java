/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.exception;

/**
 * @author ����
 *
 */
public class PunkRockStarException extends StringInstrumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public PunkRockStarException() {
	}

	/**
	 * @param arg0
	 */
	public PunkRockStarException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public PunkRockStarException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public PunkRockStarException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public PunkRockStarException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
