/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.StringInstrumentDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.StringInstrumentDTO;

/**
 * @author Назік
 *
 */
public class StringInstrumentDAOImpl extends AbstractDAO<StringInstrumentDTO, Integer> implements StringInstrumentDAO {

	@SuppressWarnings("unchecked")
	public List<StringInstrumentDTO> findAll() {
		return (List<StringInstrumentDTO>) super.findAll(StringInstrumentDTO.class);
	}

	public StringInstrumentDTO findByID(Integer id) {
		return super.findByID(StringInstrumentDTO.class, id);
	}

}
