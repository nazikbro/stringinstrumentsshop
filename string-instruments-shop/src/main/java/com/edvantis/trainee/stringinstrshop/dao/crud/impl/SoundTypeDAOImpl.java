/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.SoundTypeDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.SoundTypeDTO;

/**
 * @author Назік
 *
 */
public class SoundTypeDAOImpl extends AbstractDAO<SoundTypeDTO, Integer> implements SoundTypeDAO {

	@SuppressWarnings("unchecked")
	public List<SoundTypeDTO> findAll() {
		return (List<SoundTypeDTO>) super.findAll(SoundTypeDTO.class);
	}

	public SoundTypeDTO findByID(Integer id) {
		return super.findByID(SoundTypeDTO.class, id);
	}

}
