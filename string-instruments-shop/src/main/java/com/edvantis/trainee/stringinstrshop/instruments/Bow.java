/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

import com.edvantis.trainee.stringinstrshop.enums.BodyMaterial;

/**
 * @author ����
 *
 */
public class Bow {

	private long id;
	
	private BodyMaterial bodyMaterial;
	private double length;
	
	public Bow(long id) {
		setId(id);
		this.bodyMaterial = BodyMaterial.WOOD;
		this.length = 1;
	}
	
	/**
	 * 
	 * @param bodyMaterial
	 * @param length
	 */
	public Bow(long id, BodyMaterial bodyMaterial, double length) {
		this(id);
		this.bodyMaterial = bodyMaterial;
		this.length = length;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the bodyMaterial
	 */
	public BodyMaterial getBodyMaterial() {
		return bodyMaterial;
	}
	/**
	 * @param bodyMaterial the bodyMaterial to set
	 */
	public void setBodyMaterial(BodyMaterial bodyMaterial) {
		this.bodyMaterial = bodyMaterial;
	}
	/**
	 * @return the length
	 */
	public double getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(double length) {
		this.length = length;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bow [bodyMaterial=" + bodyMaterial + ", length=" + length + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bodyMaterial == null) ? 0 : bodyMaterial.hashCode());
		long temp;
		temp = Double.doubleToLongBits(length);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bow other = (Bow) obj;
		if (bodyMaterial != other.bodyMaterial)
			return false;
		if (Double.doubleToLongBits(length) != Double
				.doubleToLongBits(other.length))
			return false;
		return true;
	}
	 
	

}
