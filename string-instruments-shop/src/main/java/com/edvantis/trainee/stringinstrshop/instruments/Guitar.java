/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.edvantis.trainee.stringinstrshop.enums.BodyMaterial;
import com.edvantis.trainee.stringinstrshop.enums.SizeType;
import com.edvantis.trainee.stringinstrshop.enums.SoundType;
import com.edvantis.trainee.stringinstrshop.enums.StringMaterial;
import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;

/**
 * @author ����
 *
 */
public class Guitar extends StringInstrument {

	private int modeCount;
	private static Logger logger = LogManager.getLogger();

	public Guitar() {
		super();
	}

	/**
	 * Constructor for acoustic guitar
	 * 
	 * @param manufacturer
	 * @param modelName
	 * @param price
	 * @param stringCount
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws EmptyDataException
	 */
	public Guitar(long id, String manufacturer, String modelName, float price, int stringCount)
			throws DataValidityException, DataIntegrityException, EmptyDataException {
		super(id, manufacturer, modelName, price, SizeType.BIG, BodyMaterial.WOOD, SoundType.ACOUSTIC,
				StringMaterial.STEEL, stringCount);
		this.modeCount = 20;
	}

	/**
	 * @param manufacturer
	 * @param modelName
	 * @param price
	 * @param sizeType
	 * @param bodyMaterials
	 * @param soundType
	 * @param stringMaterial
	 * @param stringCount
	 * @param modeCount
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws EmptyDataException
	 */
	public Guitar(long id, String manufacturer, String modelName, float price, SizeType sizeType,
			BodyMaterial bodyMaterials, SoundType soundType, StringMaterial stringMaterial, int stringCount,
			int modeCount) throws DataValidityException, DataIntegrityException, EmptyDataException {
		super(id, manufacturer, modelName, price, sizeType, bodyMaterials, soundType, stringMaterial, stringCount);
		this.modeCount = modeCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#tune()
	 */
	@Override
	public void tune() {
		logger.info("Guitar " + super.getManufacturer() + " " + super.getModelName() + " has tuned");
		super.setTuned(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#pick()
	 */
	@Override
	protected void pick() {
		logger.info("Note on a guitar has picked");

	}

	public int getModeCount() {
		return modeCount;
	}

	public void setModeCount(int modeCount) {
		this.modeCount = modeCount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + modeCount;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Guitar other = (Guitar) obj;
		if (modeCount != other.modeCount)
			return false;
		return true;
	}

}
