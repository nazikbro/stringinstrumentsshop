/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import org.hibernate.Query;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.BodyMaterialDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.BodyMaterialDTO;
import com.edvantis.trainee.stringinstrshop.dao.util.HibernateUtil;

/**
 * @author Назік
 *
 */
public class BodyMaterialDAOImpl extends AbstractDAO<BodyMaterialDTO, Integer> implements BodyMaterialDAO {

	public BodyMaterialDTO findByName(String name) {
		String sql = "FROM BodyMaterialDTO WHERE bodyMaterialName = :name";
		Query query = HibernateUtil.getSession().createQuery(sql).setString("name", name);
		BodyMaterialDTO bodyMaterialDTO = findOne(query);
		return bodyMaterialDTO;
	}

	@SuppressWarnings("unchecked")
	public List<BodyMaterialDTO> findAll() {
		return (List<BodyMaterialDTO>) super.findAll(BodyMaterialDTO.class);
	}

	public BodyMaterialDTO findByID(Integer id) {
		return super.findByID(BodyMaterialDTO.class, id);
	}

}
