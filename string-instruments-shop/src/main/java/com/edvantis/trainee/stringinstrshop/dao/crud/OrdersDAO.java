/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.OrdersDTO;

/**
 * @author Назік
 *
 */
public interface OrdersDAO extends GenericDAO<OrdersDTO, Integer> {
	public List<OrdersDTO> findAll();

	public OrdersDTO findByID(Integer id);
}
