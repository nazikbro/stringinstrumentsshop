package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.BowDTO;

public interface BowDAO extends GenericDAO<BowDTO, Integer> {

	public List<BowDTO> findAll();

	public BowDTO findByID(Integer id);
}
