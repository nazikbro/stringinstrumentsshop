/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.util;

import org.hibernate.cfg.reveng.DelegatingReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.ReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.TableIdentifier;

/**
 * @author Назік
 *
 */
public class StrIntrShopReverseEngineeringStrategy extends DelegatingReverseEngineeringStrategy {

	public StrIntrShopReverseEngineeringStrategy(ReverseEngineeringStrategy s) {
		super(s);
	}

	public String tableToClassName(TableIdentifier tableIdentifier) {
		String className = super.tableToClassName(tableIdentifier);
		return className + "DTO";
	}
}
