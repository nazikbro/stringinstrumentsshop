/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.SoundTypeDTO;

/**
 * @author Назік
 *
 */
public interface SoundTypeDAO extends GenericDAO<SoundTypeDTO, Integer> {
	public List<SoundTypeDTO> findAll();

	public SoundTypeDTO findByID(Integer id);
}
