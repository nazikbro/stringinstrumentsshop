/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.OccupationDTO;

/**
 * @author Назік
 *
 */
public interface OccupationDAO extends GenericDAO<OccupationDTO, Integer> {
	public List<OccupationDTO> findAll();

	public OccupationDTO findByID(Integer id);
}
