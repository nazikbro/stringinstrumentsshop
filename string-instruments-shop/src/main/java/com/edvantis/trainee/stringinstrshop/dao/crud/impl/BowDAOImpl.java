/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.BowDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.BowDTO;

/**
 * @author Назік
 *
 */
public class BowDAOImpl extends AbstractDAO<BowDTO, Integer> implements BowDAO {

	@SuppressWarnings("unchecked")
	public List<BowDTO> findAll() {
		return (List<BowDTO>) super.findAll(BowDTO.class);
	}

	public BowDTO findByID(Integer id) {
		return super.findByID(BowDTO.class, id);
	}

}
