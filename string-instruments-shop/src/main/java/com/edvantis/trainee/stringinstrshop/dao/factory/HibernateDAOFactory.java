/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.factory;

import com.edvantis.trainee.stringinstrshop.dao.crud.GenericDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.BodyMaterialDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.BowDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.InstrumentTypeDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.OccupationDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.OrdersDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.PersonDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.SizeTypeDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.SoundTypeDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.StringInstrumentDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.StringTypeDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.enums.StringInstrumentDAOType;
import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * Return DAO instance
 * 
 * @author Назік
 *
 */
public class HibernateDAOFactory implements DAOFactory {
	public GenericDAO<?, Integer> getDAO(StringInstrumentDAOType type) throws UnknownTypeException {

		switch (type) {
		case BODY_MATERIAL:
			return new BodyMaterialDAOImpl();
		case BOW:
			return new BowDAOImpl();
		case INSTRUMENT_TYPE:
			return new InstrumentTypeDAOImpl();
		case OCCUPATION:
			return new OccupationDAOImpl();
		case ORDERS:
			return new OrdersDAOImpl();
		case PERSON:
			return new PersonDAOImpl();
		case SIZY_TYPE:
			return new SizeTypeDAOImpl();
		case SOUND_TYPE:
			return new SoundTypeDAOImpl();
		case STRING_TYPE:
			return new StringInstrumentDAOImpl();
		case STRING_INSTRUMENT:
			return new StringTypeDAOImpl();
		}
		throw new UnknownTypeException();

	}

}
