/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.person;

import com.edvantis.trainee.stringinstrshop.enums.Occupation;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;

/**
 * @author ����
 *
 */
public class Worker extends Person {

	private Occupation occupation;

	public Worker() {
	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 * @param occupation
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 */
	public Worker(long id, String firstName, String lastName, String email, String password, Occupation occupation)
			throws EmptyDataException, DataValidityException {
		super(id, firstName, lastName, email, password);
		setOccupation(occupation);
	}

	/**
	 * @return the Occupation
	 */
	public Occupation getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation
	 *            the Occupation to set
	 */
	public void setOccupation(Occupation occupation) {
		this.occupation = occupation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [ First Name =" + getFirstName() + ", Last Name = " + getLastName() + ", Email = " + getEmail()
				+ "Occupation = " + getOccupation() + "]";
	}
}
