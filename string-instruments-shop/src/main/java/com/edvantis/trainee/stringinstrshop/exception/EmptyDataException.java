/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.exception;

/**
 * @author ����
 *
 */
public class EmptyDataException extends StringInstrumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public EmptyDataException() {
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EmptyDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EmptyDataException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public EmptyDataException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public EmptyDataException(Throwable cause) {
		super(cause);
	}

}
