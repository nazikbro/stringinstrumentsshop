/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.OccupationDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.OccupationDTO;

/**
 * @author Назік
 *
 */
public class OccupationDAOImpl extends AbstractDAO<OccupationDTO, Integer> implements OccupationDAO {

	@SuppressWarnings("unchecked")
	public List<OccupationDTO> findAll() {
		return (List<OccupationDTO>) super.findAll(OccupationDTO.class);
	}

	public OccupationDTO findByID(Integer id) {
		return super.findByID(OccupationDTO.class, id);
	}

}
