package com.edvantis.trainee.stringinstrshop.storage;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.InstrumentNotFoundExeption;
import com.edvantis.trainee.stringinstrshop.exception.OutOfStockException;
import com.edvantis.trainee.stringinstrshop.exception.StringInstrumentException;
import com.edvantis.trainee.stringinstrshop.exception.WrongIndexException;
import com.edvantis.trainee.stringinstrshop.instruments.StringInstrument;
import com.edvantis.trainee.stringinstrshop.order.Order;

public class MusicInstrumentsStorage {
	private static Logger logger = LogManager.getLogger();

	private static volatile MusicInstrumentsStorage instace;

	private List<StringInstrument> instrumentList;
	private List<Order> orderList;
	private double profit = 0;

	/**
	 * Private constructor.
	 * 
	 */
	private MusicInstrumentsStorage() {
		instrumentList = new ArrayList<StringInstrument>();
		orderList = new ArrayList<Order>();
	}

	/**
	 * 
	 * @return instance of musicinstrumentsStorage.
	 */
	public static MusicInstrumentsStorage getInstance() {
		if (instace == null) {
			synchronized (MusicInstrumentsStorage.class) {
				if (instace == null) {
					instace = new MusicInstrumentsStorage();
				}
			}
		}
		return instace;
	}

	/**
	 * 
	 * @param instrument
	 *            .
	 * @throws DataIntegrityException
	 */
	public void addInstrument(StringInstrument instrument) throws DataIntegrityException {
		if (isContains(instrument.getId(), instrumentList)) {
			throw logger.throwing(new DataIntegrityException("Instrument already exist in storage"));
		}
		instrumentList.add(instrument);

	}

	/**
	 * @return the profit.
	 */
	public double getProfit() {
		return profit;
	}

	public int getInstrumentsCount() {
		return instrumentList.size();
	}

	/**
	 * 
	 * @param order
	 *            .
	 * @throws EmptyDataException .
	 */
	public void addOrder(Order order) throws EmptyDataException {
		if (order == null) {
			throw logger.throwing(new EmptyDataException());
		}
		orderList.add(order);
	}

	/**
	 * 
	 * @param index
	 * @return
	 * @throws WrongIndexException .
	 * @throws EmptyDataException .
	 */
	public Order getOrder(int index) throws WrongIndexException, EmptyDataException {
		if (index >= 0 && index < orderList.size()) {
			return orderList.get(index);
		} else {
			throw logger.throwing(new WrongIndexException());
		}
	}

	/**
	 * Commit all orders in storage
	 * 
	 * @throws StringInstrumentException
	 */
	public void commitAllOrders() throws StringInstrumentException {
		for (Order order : orderList) {
			if (!order.isCommitted()) {
				commit(order);
			}
		}
	}

	public void commitOrder(Order order) throws StringInstrumentException {
		if (order == null) {
			throw logger.throwing(new EmptyDataException());
		}
		if (orderList.contains(order)) {
			if (!order.isCommitted()) {
				commit(order);
			}
		} else {
			throw logger.throwing(new StringInstrumentException("Order not found in order list"));
		}

	}

	public int getOrderListSize() throws EmptyDataException {
		return orderList.size();
	}

	@Override
	public String toString() {
		return "MusicInstrumentsStorage = " + instrumentList.size() + ", profit = " + profit + "]";
	}

	private void commit(Order order) throws StringInstrumentException {
		for (StringInstrument instrument : order.getOrderInstruments()) {
			sellInstrument(instrument);
		}
		order.setCommitted(true);
	}

	/**
	 * Retrieving string instrument from list by instrument ID
	 * 
	 * @param id
	 * @param list
	 * @return
	 * @throws WrongIndexException
	 * @throws InstrumentNotFoundExeption
	 */
	public StringInstrument getInstrumentById(long id) throws InstrumentNotFoundExeption {
		logger.entry();
		for (StringInstrument t : instrumentList) {
			if (t.getId() == id) {
				return logger.exit(t);
			}
		}
		throw logger.throwing(new InstrumentNotFoundExeption());
	}

	/**
	 * 
	 * @param id
	 * @param list
	 * @return
	 * @throws WrongIndexException
	 * @throws InstrumentNotFoundExeption
	 */
	private boolean isContains(long id, List<? extends StringInstrument> list) {
		logger.entry();
		for (StringInstrument t : list) {
			if (t.getId() == id) {
				return logger.exit(true);
			}
		}
		return logger.exit(false);
	}

	private void sellInstrument(StringInstrument instrument) throws InstrumentNotFoundExeption, DataValidityException,
			OutOfStockException {

		if (!instrumentList.contains(instrument)) {
			throw logger.throwing(new InstrumentNotFoundExeption());
		}

		int index = instrumentList.indexOf(instrument);
		instrumentList.get(index).setSold(true);
		logger.info("Selling performed");
		profit += instrument.getPrice();

	}

}
