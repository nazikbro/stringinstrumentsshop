/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.SizeTypeDTO;

/**
 * @author Назік
 *
 */
public interface SizeTypeDAO extends GenericDAO<SizeTypeDTO, Integer> {
	public List<SizeTypeDTO> findAll();

	public SizeTypeDTO findByID(Integer id);
}
