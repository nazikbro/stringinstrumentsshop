/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.exception;

/**
 * @author ����
 *
 */
public class StringInstrumentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public StringInstrumentException() {
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public StringInstrumentException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public StringInstrumentException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public StringInstrumentException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public StringInstrumentException(Throwable cause) {
		super(cause);
	}

}
