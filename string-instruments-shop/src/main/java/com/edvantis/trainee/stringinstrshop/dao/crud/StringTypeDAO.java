/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.StringTypeDTO;

/**
 * @author Назік
 *
 */
public interface StringTypeDAO extends GenericDAO<StringTypeDTO, Integer> {
	public List<StringTypeDTO> findAll();

	public StringTypeDTO findByID(Integer id);
}
