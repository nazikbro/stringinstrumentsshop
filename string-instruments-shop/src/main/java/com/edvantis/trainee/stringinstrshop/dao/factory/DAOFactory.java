/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.factory;

import com.edvantis.trainee.stringinstrshop.dao.crud.GenericDAO;
import com.edvantis.trainee.stringinstrshop.dao.enums.StringInstrumentDAOType;
import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * @author Назік
 *
 */
public interface DAOFactory {

	public GenericDAO<?, Integer> getDAO(StringInstrumentDAOType type) throws UnknownTypeException;
}
