/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.InstrumentTypeDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.InstrumentTypeDTO;

/**
 * @author Назік
 *
 */
public class InstrumentTypeDAOImpl extends AbstractDAO<InstrumentTypeDTO, Integer> implements InstrumentTypeDAO {

	@SuppressWarnings("unchecked")
	public List<InstrumentTypeDTO> findAll() {
		return (List<InstrumentTypeDTO>) super.findAll(InstrumentTypeDTO.class);
	}

	public InstrumentTypeDTO findByID(Integer id) {
		return super.findByID(InstrumentTypeDTO.class, id);
	}

}
