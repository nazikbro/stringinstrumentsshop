/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.person;

import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * @author ����
 *
 */
public class PersonFactory {
	
	/**
	 * Factory method - create object by passing its type.
	 * 
	 * @param type
	 * @return
	 * @throws UnknownTypeException
	 */
	public Person getInstrument(String type) throws UnknownTypeException {
		if (type.equalsIgnoreCase("Worker")) {
			return new Worker();
		}
		if (type.equalsIgnoreCase("Client")) {
			return new Client();
		}
		throw new UnknownTypeException("Wrong type of object");
	}
}
