/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.person;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.utill.EmailValidator;

/**
 * @author ����
 *
 */
public class Person {

	private static Logger logger = LogManager.getLogger();

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;

	public Person() {
	}

	/**
	 * Constructor for clients
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 */
	public Person(long id, String firstName, String lastName, String email, String password) throws EmptyDataException,
			DataValidityException {
		setId(id);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setPassword(password);
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 * @throws EmptyDataException
	 */
	public void setFirstName(String firstName) throws EmptyDataException {
		if (!firstName.isEmpty())
			this.firstName = firstName;
		else
			throw logger.throwing(new EmptyDataException("firstName didn`t valid"));

	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 * @throws EmptyDataException
	 */
	public void setLastName(String lastName) throws EmptyDataException {
		if (!lastName.isEmpty())
			this.lastName = lastName;
		else
			throw logger.throwing(new EmptyDataException("lastName didn`t valid"));
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 * @throws DataValidityException
	 */
	public void setEmail(String email) throws DataValidityException {
		if (EmailValidator.validate(email))
			this.email = email;
		else
			throw logger.throwing(new DataValidityException("email didn`t valid"));
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 * @throws DataValidityException
	 */
	public void setPassword(String password) throws DataValidityException {
		if (password.length() >= 4)
			this.password = password;
		else
			throw logger.throwing(new DataValidityException("password didn`t valid"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PersonDTO [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}

}
