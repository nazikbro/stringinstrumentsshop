/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.InstrumentTypeDTO;

/**
 * @author Назік
 *
 */
public interface InstrumentTypeDAO extends GenericDAO<InstrumentTypeDTO, Integer> {
	public List<InstrumentTypeDTO> findAll();

	public InstrumentTypeDTO findByID(Integer id);

}
