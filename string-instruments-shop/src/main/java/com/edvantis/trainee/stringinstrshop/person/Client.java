/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.person;

import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;

/**
 * @author ����
 *
 */
public class Client extends Person {

	public Client() {
	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 */
	public Client(long id, String firstName, String lastName, String email, String password) throws EmptyDataException,
			DataValidityException {
		super(id, firstName, lastName, email, password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [ First Name =" + getFirstName() + ", Last Name = " + getLastName() + ", Email = " + getEmail()
				+ "]";
	}

}
