/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import java.util.List;

import org.hibernate.Query;

import com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.PersonDAO;
import com.edvantis.trainee.stringinstrshop.dao.entities.PersonDTO;
import com.edvantis.trainee.stringinstrshop.dao.util.HibernateUtil;

/**
 * @author Назік
 *
 */
public class PersonDAOImpl extends AbstractDAO<PersonDTO, Integer> implements PersonDAO {

	public PersonDTO findByName(String firstName, String lastName) {
		String sql = "FROM PersonDTO WHERE firstName = :firstName AND lastName = :lastName";
		Query query = HibernateUtil.getSession().createQuery(sql).setString("firstName", firstName)
				.setString("lastName", lastName);
		PersonDTO person = findOne(query);
		return person;
	}

	@SuppressWarnings("unchecked")
	public List<PersonDTO> findAll() {
		return (List<PersonDTO>) super.findAll(PersonDTO.class);
	}

	public PersonDTO findByID(Integer id) {
		return super.findByID(PersonDTO.class, id);
	}
}
