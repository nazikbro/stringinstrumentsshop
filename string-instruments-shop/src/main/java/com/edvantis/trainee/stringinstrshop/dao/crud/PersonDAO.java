/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud;

import java.util.List;

import com.edvantis.trainee.stringinstrshop.dao.entities.PersonDTO;

/**
 * @author Назік
 *
 */
public interface PersonDAO extends GenericDAO<PersonDTO, Integer> {

	public PersonDTO findByName(String firstName, String lastName);

	public List<PersonDTO> findAll();

	public PersonDTO findByID(Integer id);
}
