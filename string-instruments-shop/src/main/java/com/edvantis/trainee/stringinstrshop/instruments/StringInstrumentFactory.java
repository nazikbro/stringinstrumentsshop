/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * @author ����
 *
 */
public class StringInstrumentFactory {

	/**
	 * Factory method - create object by passing its type.
	 * 
	 * @param type
	 * @return
	 * @throws UnknownTypeException
	 */
	public StringInstrument getInstrument(String type) throws UnknownTypeException {
		if (type.equalsIgnoreCase("Guitar")) {
			return new Guitar();
		}
		if (type.equalsIgnoreCase("Violin")) {
			return new Violin();
		}
		if (type.equalsIgnoreCase("DoubleBass")) {
			return new DoubleBass();
		}
		throw new UnknownTypeException("Wrong type of object");
	}
}
