/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

/**
 * @author ����
 *
 */
public interface Bowable {
	public void bow(Bow bow);
}
