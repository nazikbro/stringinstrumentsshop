/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

import com.edvantis.trainee.stringinstrshop.enums.BodyMaterial;
import com.edvantis.trainee.stringinstrshop.enums.SizeType;
import com.edvantis.trainee.stringinstrshop.enums.SoundType;
import com.edvantis.trainee.stringinstrshop.enums.StringMaterial;
import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.PunkRockStarException;

/**
 * @author ����
 *
 */
public abstract class StringInstrument {

	private long id;
	private SizeType sizeType;
	private BodyMaterial bodyMaterials;
	private SoundType soundType;
	private StringMaterial stringMaterial;

	private int stringCount;
	private String manufacturer;
	private String modelName;
	private float price;
	private boolean isTuned = false;
	private boolean isSold = false;
	

	public StringInstrument() {
	}

	/**
	 * @param id
	 * @param amount
	 * @param manufacturer
	 * @param modelName
	 * @param price
	 * @param sizeType
	 * @param bodyMaterials
	 * @param soundType
	 * @param stringMaterial
	 * @param stringCount
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws EmptyDataException
	 */
	public StringInstrument(long id, String manufacturer, String modelName, float price, SizeType sizeType,
			BodyMaterial bodyMaterials, SoundType soundType, StringMaterial stringMaterial, int stringCount)
			throws DataValidityException, DataIntegrityException, EmptyDataException {
		setId(id);
		setManufacturer(manufacturer);
		setModelName(modelName);
		setPrice(price);
		setStringCount(stringCount);
		setSizeType(sizeType);
		setSoundType(soundType);
		setBodyMaterials(bodyMaterials);
		setStringMaterial(stringMaterial);
	}

	public abstract void tune();

	protected abstract void pick();

	protected void useTechnique() {
	}

	protected boolean isUseTechnique() {
		return false;
	}

	public final void play() throws PunkRockStarException {
		if (!isTuned())
			throw new PunkRockStarException();
		else if (!isUseTechnique())
			pick();
		else
			useTechnique();
	}

	@Override
	public String toString() {
		return "StringInstrumentDTO [manufacturer=" + manufacturer + ", modelName=" + modelName + ", price=" + price + "]";
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * getters and setters next
	 */

	public SizeType getSizeType() {
		return sizeType;
	}

	public void setSizeType(SizeType sizeType) {
		this.sizeType = sizeType;
	}

	public BodyMaterial getBodyMaterials() {
		return bodyMaterials;
	}

	/**
	 * 
	 * @param bodyMaterials
	 * @throws DataIntegrityException
	 */
	public void setBodyMaterials(BodyMaterial bodyMaterials) throws DataIntegrityException {

		if (bodyMaterials != BodyMaterial.WOOD && soundType == SoundType.ACOUSTIC)
			throw new DataIntegrityException("Acoustic guitar must be made with wood");
		this.bodyMaterials = bodyMaterials;
	}

	public SoundType getSoundType() {
		return soundType;
	}

	public void setSoundType(SoundType soundType) {
		this.soundType = soundType;
	}

	public StringMaterial getStringMaterial() {
		return stringMaterial;
	}

	public void setStringMaterial(StringMaterial stringMaterial) {
		this.stringMaterial = stringMaterial;
	}

	public int getStringCount() {
		return stringCount;
	}

	/**
	 * 
	 * @param stringCount
	 * @throws DataValidityException
	 */
	public void setStringCount(int stringCount) throws DataValidityException {
		if (stringCount > 0)
			this.stringCount = stringCount;
		else
			throw new DataValidityException("String count not valid");

	}

	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * 
	 * @param manufacturer
	 * @throws EmptyDataException
	 */
	public void setManufacturer(String manufacturer) throws EmptyDataException {
		if (!manufacturer.isEmpty())
			this.manufacturer = manufacturer;
		else
			throw new EmptyDataException("Manufacturer name is empty");

	}

	public String getModelName() {
		return modelName;
	}

	/**
	 * 
	 * @param modelName
	 * @throws EmptyDataException
	 */
	public void setModelName(String modelName) throws EmptyDataException {
		if (!modelName.isEmpty())
			this.modelName = modelName;
		else
			throw new EmptyDataException("Model name is empty");

	}

	public float getPrice() {
		return price;
	}

	/**
	 * 
	 * @param price
	 * @throws DataValidityException
	 */
	public void setPrice(float price) throws DataValidityException {
		if (price > 0)
			this.price = price;
		else
			throw new DataValidityException("Price not valid");
	}


	/**
	 * @return the isTuned
	 */
	public boolean isTuned() {
		return isTuned;
	}

	/**
	 * @param isTuned
	 *            the isTuned to set
	 */
	void setTuned(boolean isTuned) {
		this.isTuned = isTuned;
	}

	/**
	 * @return the isSold
	 */
	public boolean isSold() {
		return isSold;
	}

	/**
	 * @param isSold the isSold to set
	 */
	public void setSold(boolean isSold) {
		this.isSold = isSold;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bodyMaterials == null) ? 0 : bodyMaterials.hashCode());
		result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((modelName == null) ? 0 : modelName.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((sizeType == null) ? 0 : sizeType.hashCode());
		result = prime * result + ((soundType == null) ? 0 : soundType.hashCode());
		result = prime * result + stringCount;
		result = prime * result + ((stringMaterial == null) ? 0 : stringMaterial.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringInstrument other = (StringInstrument) obj;
		if (bodyMaterials != other.bodyMaterials)
			return false;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (modelName == null) {
			if (other.modelName != null)
				return false;
		} else if (!modelName.equals(other.modelName))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		if (sizeType != other.sizeType)
			return false;
		if (soundType != other.soundType)
			return false;
		if (stringCount != other.stringCount)
			return false;
		if (stringMaterial != other.stringMaterial)
			return false;
		return true;
	}

}
