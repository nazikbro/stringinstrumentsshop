package com.edvantis.trainee.stringinstrshop.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.edvantis.trainee.stringinstrshop.enums.BodyMaterial;
import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.PunkRockStarException;
import com.edvantis.trainee.stringinstrshop.instruments.Bow;
import com.edvantis.trainee.stringinstrshop.instruments.DoubleBass;
import com.edvantis.trainee.stringinstrshop.instruments.Guitar;
import com.edvantis.trainee.stringinstrshop.instruments.StringInstrument;
import com.edvantis.trainee.stringinstrshop.instruments.Violin;
import com.edvantis.trainee.stringinstrshop.storage.MusicInstrumentsStorage;

public class ShopTest {
	private static Logger logger = LogManager.getLogger();

	public static void main(String[] args) {
		logger.entry();
		try {
			StringInstrument violin = new Violin(1, "Stradivari", "MegaViolin", 333000, new Bow(1, BodyMaterial.WOOD,
					0.9));
			violin.tune();
			violin.play();
		} catch (PunkRockStarException e) {
			logger.catching(e);
		} catch (DataValidityException e) {
			logger.catching(e);
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}

		MusicInstrumentsStorage storage = MusicInstrumentsStorage.getInstance();
		try {
			storage.addInstrument(new Violin(1, "Stradivari", "Violin", 333000, new Bow(1, BodyMaterial.WOOD, 0.9)));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new Violin(1, "Stradivari", "HihaViolin", 325000, new Bow(1, BodyMaterial.WOOD, 0.8)));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new Violin(1, "Stradivari", "MegaViolin2", 300000, new Bow(1, BodyMaterial.WOOD, 0.9)));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new Violin(1, "Stradivari", "MegaViolin1", 399650, new Bow(1, BodyMaterial.WOOD, 0.9)));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}

		try {
			storage.addInstrument(new Guitar(1, "Gibson", "j-45", 68000, 6));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new Guitar(1, "Gibson", "j-35", 75600, 6));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new Guitar(1, "Fender", "cd-60", 4246, 6));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new Guitar(1, "Fender", "cd-60ce", 7046, 6));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}

		try {
			storage.addInstrument(new DoubleBass(1, "Yamaha", "h1", 7008, 4, new Bow(1)));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		try {
			storage.addInstrument(new DoubleBass(1, "Yamaha", "h2", 8808, 4, new Bow(1)));
		} catch (DataValidityException e) {
			e.printStackTrace();
		} catch (DataIntegrityException e) {
			logger.catching(e);
		} catch (EmptyDataException e) {
			e.printStackTrace();
		}
		logger.trace("-----------------");
		logger.info("Storrage at start: " + storage);

		logger.info("Storrage at end: " + storage);
		logger.trace("-----------------");
		logger.exit();
	}

}
