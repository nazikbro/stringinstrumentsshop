package com.edvantis.trainee.stringinstrshop.enums;

public enum SizeType {
	BIG, MEDIUM, SMALL
}
