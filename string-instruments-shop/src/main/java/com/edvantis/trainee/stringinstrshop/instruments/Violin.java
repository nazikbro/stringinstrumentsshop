/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.edvantis.trainee.stringinstrshop.enums.BodyMaterial;
import com.edvantis.trainee.stringinstrshop.enums.SizeType;
import com.edvantis.trainee.stringinstrshop.enums.SoundType;
import com.edvantis.trainee.stringinstrshop.enums.StringMaterial;
import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;

/**
 * @author ����
 *
 */
public class Violin extends StringInstrument implements Bowable {

	private static Logger logger = LogManager.getLogger();
	private Bow bow;

	public Violin() {
		super();
	}

	/**
	 * Constructor for acoustic violin
	 * 
	 * @param manufacturer
	 * @param modelName
	 * @param price
	 * @param bow
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws EmptyDataException
	 */
	public Violin(long id, String manufacturer, String modelName, float price, Bow bow) throws DataValidityException,
			DataIntegrityException, EmptyDataException {
		super(id, manufacturer, modelName, price, SizeType.BIG, BodyMaterial.WOOD, SoundType.ACOUSTIC,
				StringMaterial.STEEL, 4);
		this.bow = bow;
	}

	/**
	 * @param manufacturer
	 * @param modelName
	 * @param price
	 * @param sizeType
	 * @param bodyMaterials
	 * @param soundType
	 * @param stringMaterial
	 * @param bow
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws EmptyDataException
	 */
	public Violin(long id, String manufacturer, String modelName, float price, SizeType sizeType,
			BodyMaterial bodyMaterials, SoundType soundType, StringMaterial stringMaterial, Bow bow)
			throws DataValidityException, DataIntegrityException, EmptyDataException {
		super(id, manufacturer, modelName, price, sizeType, bodyMaterials, soundType, stringMaterial, 4);
		this.bow = bow;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#
	 * setStringCount(int)
	 */
	@Override
	public void setStringCount(int stringCount) throws DataValidityException {
		if (stringCount != 4)
			throw new DataValidityException("Cannot change string count in violin");
		else
			super.setStringCount(stringCount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#tune()
	 */
	@Override
	public void tune() {
		logger.info("Violin " + super.getManufacturer() + " " + super.getModelName() + " has tuned");
		super.setTuned(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#pick()
	 */
	@Override
	protected void pick() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#
	 * useTechnique()
	 */
	@Override
	protected void useTechnique() {
		bow(new Bow(1, BodyMaterial.WOOD, 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#
	 * isUseTechnique()
	 */
	@Override
	protected boolean isUseTechnique() {
		return true;
	}

	public void bow(Bow bow) {
		logger.info("Note on violin has bowen with " + bow.toString());
	}

	/**
	 * @return the bow
	 */
	public Bow getBow() {
		return bow;
	}

	/**
	 * @param bow
	 *            the bow to set
	 */
	public void setBow(Bow bow) {
		this.bow = bow;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bow == null) ? 0 : bow.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Violin other = (Violin) obj;
		if (bow == null) {
			if (other.bow != null)
				return false;
		} else if (!bow.equals(other.bow))
			return false;
		return true;
	}

}
