/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.exception;

/**
 * @author ����
 *
 */
public class WrongIndexException extends StringInstrumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public WrongIndexException() {
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public WrongIndexException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WrongIndexException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public WrongIndexException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public WrongIndexException(Throwable cause) {
		super(cause);
	}

}
