/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.exception;

/**
 * @author ����
 *
 */
public class DataIntegrityException extends StringInstrumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public DataIntegrityException() {
	}

	/**
	 * @param arg0
	 */
	public DataIntegrityException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public DataIntegrityException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DataIntegrityException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public DataIntegrityException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
