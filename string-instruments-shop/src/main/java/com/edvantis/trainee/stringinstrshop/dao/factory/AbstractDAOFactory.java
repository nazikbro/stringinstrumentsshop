/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.factory;

import com.edvantis.trainee.stringinstrshop.dao.crud.GenericDAO;
import com.edvantis.trainee.stringinstrshop.dao.enums.StringInstrumentDAOType;
import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * @author Назік
 *
 */
public class AbstractDAOFactory {
	private  DAOFactory factory;

	/**
	 * @param factory
	 */
	public AbstractDAOFactory(DAOFactory factory) {
		this.factory = factory;
	}
	
	public  GenericDAO<?, Integer> getDAO(StringInstrumentDAOType type) throws UnknownTypeException {

		return factory.getDAO(type);

	}

}
