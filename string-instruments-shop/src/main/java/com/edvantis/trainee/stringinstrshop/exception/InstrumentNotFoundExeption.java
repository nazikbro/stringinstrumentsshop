package com.edvantis.trainee.stringinstrshop.exception;

public class InstrumentNotFoundExeption extends StringInstrumentException {

	private static final long serialVersionUID = 1L;

	public InstrumentNotFoundExeption() {
	}

	public InstrumentNotFoundExeption(String arg0) {
		super(arg0);
	}

	public InstrumentNotFoundExeption(Throwable arg0) {
		super(arg0);
	}

	public InstrumentNotFoundExeption(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InstrumentNotFoundExeption(String arg0, Throwable arg1,
			boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
