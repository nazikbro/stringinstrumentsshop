/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.order;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.InstrumentNotFoundExeption;
import com.edvantis.trainee.stringinstrshop.exception.WrongIndexException;
import com.edvantis.trainee.stringinstrshop.instruments.StringInstrument;
import com.edvantis.trainee.stringinstrshop.person.Client;
import com.edvantis.trainee.stringinstrshop.person.Person;
import com.edvantis.trainee.stringinstrshop.person.Worker;
import com.google.common.collect.ImmutableList;

/**
 * @author ����
 *
 */
public class Order {

	private static Logger logger = LogManager.getLogger();

	private long id;
	private Client client;
	private Worker worker;
	private List<StringInstrument> instrumentList;
	private boolean committed;
	private String orderDate;

	/**
	 * @param client
	 * @param worker
	 * @throws EmptyDataException
	 */
	public Order(long id, Person client, Person worker) throws EmptyDataException {
		setId(id);
		setClient(client);
		setWorker(worker);
		setCommitted(false);
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the client
	 */
	public Person getClient() {
		return client;
	}

	/**
	 * @param client
	 *            the client to set
	 * @throws EmptyDataException
	 */
	public void setClient(Person client) throws EmptyDataException {
		if (client != null)
			this.client = (Client) client;
		else
			throw logger.throwing(new EmptyDataException("Instrument list is null"));
	}

	/**
	 * @return the worker
	 */
	public Person getWorker() {
		return worker;
	}

	/**
	 * @param worker
	 *            the worker to set
	 * @throws EmptyDataException
	 */
	public void setWorker(Person worker) throws EmptyDataException {
		if (worker != null)
			this.worker = (Worker) worker;
		else
			throw logger.throwing(new EmptyDataException("Instrument list is null"));
	}

	/**
	 * @return the committed
	 */
	public boolean isCommitted() {
		return committed;
	}

	/**
	 * @param committed
	 *            the committed to set
	 */
	public void setCommitted(boolean committed) {
		this.committed = committed;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the quantity
	 * @throws EmptyDataException
	 */
	public int getQuantity() throws EmptyDataException {
		if (instrumentList != null) {
			return instrumentList.size();
		} else
			throw logger.throwing(new EmptyDataException());
	}

	/**
	 * @return the full Price
	 * @throws InstrumentNotFoundExeption
	 */
	public float getFullPrice() throws EmptyDataException {
		float price = 0;
		if (instrumentList == null) {
			throw logger.throwing(new EmptyDataException());
		}

		for (StringInstrument stringInstrument : instrumentList) {
			price += stringInstrument.getPrice();
		}
		return price;

	}

	/**
	 * Add instrument to list
	 * 
	 * @param instrument
	 * @throws EmptyDataException
	 */
	public void addItem(StringInstrument instrument) throws EmptyDataException {
		if (instrument != null) {
			if (instrumentList == null)
				instrumentList = new ArrayList<StringInstrument>();
			instrumentList.add(instrument);
		} else
			throw logger.throwing(new EmptyDataException("Instrument is null"));
	}


	/**
	 * remove instrument from list
	 * 
	 * @param index
	 * @throws EmptyDataException
	 * @throws WrongIndexException
	 * @throws InstrumentNotFoundExeption
	 */
	public void removeItem(StringInstrument instrument) throws EmptyDataException, WrongIndexException,
			InstrumentNotFoundExeption {
		if (instrument != null) {
			if (instrumentList.contains(instrument)) {
				instrumentList.remove(instrument);
			} else
				throw logger.throwing(new InstrumentNotFoundExeption());
		} else
			throw logger.throwing(new EmptyDataException("Instrument is null"));
	}

	/**
	 * @return the immutable list of instruments in order
	 */
	public List<StringInstrument> getOrderInstruments() {
		ImmutableList<StringInstrument> list = ImmutableList.copyOf(instrumentList);
		return list;
	}

}
