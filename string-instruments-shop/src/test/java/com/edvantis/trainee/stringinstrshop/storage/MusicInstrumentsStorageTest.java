package com.edvantis.trainee.stringinstrshop.storage;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.enums.Occupation;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.StringInstrumentException;
import com.edvantis.trainee.stringinstrshop.instruments.Bow;
import com.edvantis.trainee.stringinstrshop.instruments.DoubleBass;
import com.edvantis.trainee.stringinstrshop.instruments.StringInstrument;
import com.edvantis.trainee.stringinstrshop.order.Order;
import com.edvantis.trainee.stringinstrshop.person.Client;
import com.edvantis.trainee.stringinstrshop.person.Person;
import com.edvantis.trainee.stringinstrshop.person.Worker;

public class MusicInstrumentsStorageTest {

	Order order;
	Person client;
	Person worker;
	MusicInstrumentsStorage storage;
	ArrayList<StringInstrument> list;

	@Before
	public void init() throws EmptyDataException, DataValidityException {
		client = new Client(1, "N", "Iv", "naz@gmail.com", "12345678");
		worker = new Worker(1, "N", "Iv", "naz@gmail.com", "12345678", Occupation.WORKER);
		storage = MusicInstrumentsStorage.getInstance();

	}

	@Test
	public void testCommitAllOrders() throws StringInstrumentException {
		Order order = new Order(1, client, worker);

		DoubleBass instrument = new DoubleBass(1, "Yamaha", "h2", 8808, 4, new Bow(1));
		storage.addInstrument(instrument);
		order.addItem(instrument);

		instrument = new DoubleBass(2, "Yamaha", "h2", 8808, 4, new Bow(1));
		storage.addInstrument(instrument);
		order.addItem(instrument);

		instrument = new DoubleBass(3, "Yamaha", "h2", 8808, 4, new Bow(1));
		storage.addInstrument(instrument);
		order.addItem(instrument);

		storage.addOrder(order);
		storage.commitAllOrders();
		assertEquals(1, storage.getOrderListSize(), 0);
	}

	/**
	 * 
	 * @throws StringInstrumentException
	 */
	@Test
	public void testCommitAllOrdersList() throws StringInstrumentException {
		Order order = new Order(1, client, worker);

		DoubleBass instrument = new DoubleBass(4, "Yamaha", "h2", 8808, 4, new Bow(1));
		storage.addInstrument(instrument);
		order.addItem(instrument);

		instrument = new DoubleBass(5, "Yamaha", "h2", 8808, 4, new Bow(1));
		storage.addInstrument(instrument);
		order.addItem(instrument);

		instrument = new DoubleBass(6, "Yamaha", "h2", 8808, 4, new Bow(1));
		storage.addInstrument(instrument);
		// order.addItem(instrument);

		storage.addOrder(order);
		storage.commitAllOrders();
		assertEquals(false, storage.getInstrumentById(6).isSold());
	}
}
