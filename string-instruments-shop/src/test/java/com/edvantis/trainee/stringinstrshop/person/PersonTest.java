/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.person;

import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;

/**
 * @author ����
 *
 */
public class PersonTest {

	Person person;

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.person.Person#setFirstName(java.lang.String)}
	 * 
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = EmptyDataException.class)
	public void testSetFirstName() throws EmptyDataException, DataValidityException {
		person = new Person(1,"", "Iv", "naz@gmail.com", "12345678");
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.person.Person#setLastName(java.lang.String)}
	 * 
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = EmptyDataException.class)
	public void testSetLastName() throws EmptyDataException, DataValidityException {
		person = new Person(1,"Naz", "", "naz@gmail.com", "12345678");
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.person.Person#setEmail(java.lang.String)}
	 * 
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = DataValidityException.class)
	public void testSetEmail() throws EmptyDataException, DataValidityException {
		person = new Person(1,"Naz", "AAA", "nazgmail.com", "12345678");
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.person.Person#setPassword(java.lang.String)}
	 * .
	 * 
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = DataValidityException.class)
	public void testSetPassword() throws EmptyDataException, DataValidityException {
		person = new Person(1,"Naz", "Dd", "naz@gmail.com", "1");
	}

}
