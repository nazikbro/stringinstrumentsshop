package com.edvantis.trainee.stringinstrshop.instruments;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

public class StringInstrumentFactoryTest {

	public StringInstrumentFactory factory;

	@Before
	public void init() {
		factory = new StringInstrumentFactory();
	}

	/**
	 * 
	 * @throws UnknownTypeException
	 */
	@Test
	public void testGetInstrument() throws UnknownTypeException {
		assertTrue(factory.getInstrument("Guitar") instanceof Guitar);

	}

	/**
	 * 
	 * @throws UnknownTypeException
	 */
	@Test(expected=UnknownTypeException.class)
	public void testGetInstrumentException() throws UnknownTypeException {
		factory.getInstrument("Guiar");
		
	}

}
