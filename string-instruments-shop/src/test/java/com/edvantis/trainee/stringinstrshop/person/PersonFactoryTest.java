/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.person;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * @author ����
 *
 */
public class PersonFactoryTest {

	public PersonFactory factory;

	@Before
	public void init() {
		factory = new PersonFactory();
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.person.PersonFactory#getInstrument(java.lang.String)}
	 * .
	 * 
	 * @throws UnknownTypeException
	 */
	@Test
	public void testGetInstrument() throws UnknownTypeException {
		assertTrue(factory.getInstrument("Client") instanceof Client);
	}

}
