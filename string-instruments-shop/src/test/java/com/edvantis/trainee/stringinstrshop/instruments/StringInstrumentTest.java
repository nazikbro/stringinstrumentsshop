/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.instruments;

import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.enums.BodyMaterial;
import com.edvantis.trainee.stringinstrshop.enums.SizeType;
import com.edvantis.trainee.stringinstrshop.enums.SoundType;
import com.edvantis.trainee.stringinstrshop.enums.StringMaterial;
import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.PunkRockStarException;

/**
 * @author ����
 *
 */
public class StringInstrumentTest {

	Guitar guitar;

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#play()}
	 * .
	 * 
	 * @throws DataIntegrityException
	 * @throws DataValidityException
	 * @throws PunkRockStarException
	 * @throws EmptyDataException
	 */
	@Test(expected = PunkRockStarException.class)
	public void testPlay() throws DataValidityException, DataIntegrityException, PunkRockStarException,
			EmptyDataException {
		guitar = new Guitar(1, "Gibson", "j-45", 68000, SizeType.BIG, BodyMaterial.WOOD, SoundType.ACOUSTIC,
				StringMaterial.STEEL, 6, 24);
		guitar.play();
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#setBodyMaterials(com.edvantis.trainee.stringinstrshop.enums.BodyMaterial)}
	 * .
	 * 
	 * @throws DataIntegrityException
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = DataIntegrityException.class)
	public void testSetBodyMaterials() throws DataValidityException, DataIntegrityException, EmptyDataException {
		guitar = new Guitar(1, "Gibson", "j-45", 68000, SizeType.BIG, BodyMaterial.PLASTIC, SoundType.ACOUSTIC,
				StringMaterial.STEEL, 6, 24);
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#setModelName(java.lang.String)}
	 * .
	 * 
	 * @throws DataIntegrityException
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = EmptyDataException.class)
	public void testSetModelName() throws DataValidityException, DataIntegrityException, EmptyDataException {
		guitar = new Guitar(1, "Gibson", "", 68000, SizeType.BIG, BodyMaterial.WOOD, SoundType.ACOUSTIC,
				StringMaterial.STEEL, 6, 24);
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#setModelName(java.lang.String)}
	 * .
	 * 
	 * @throws DataIntegrityException
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = DataValidityException.class)
	public void testSetStringCount() throws DataValidityException, DataIntegrityException, EmptyDataException {
		guitar = new Guitar(1, "Gibson", "AAA", 68000, SizeType.BIG, BodyMaterial.WOOD, SoundType.ACOUSTIC,
				StringMaterial.STEEL, 0, 24);
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.instruments.StringInstrument#setPrice(float)}
	 * .
	 * 
	 * @throws DataIntegrityException
	 * @throws DataValidityException
	 * @throws EmptyDataException
	 */
	@Test(expected = DataValidityException.class)
	public void testSetPrice() throws DataValidityException, DataIntegrityException, EmptyDataException {
		guitar = new Guitar(1, "AAAA", "j-45", -68000, SizeType.BIG, BodyMaterial.WOOD, SoundType.ACOUSTIC,
				StringMaterial.STEEL, 6, 24);
	}

}
