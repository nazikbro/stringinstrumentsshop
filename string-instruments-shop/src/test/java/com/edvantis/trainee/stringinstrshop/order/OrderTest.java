/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.order;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.enums.Occupation;
import com.edvantis.trainee.stringinstrshop.exception.DataIntegrityException;
import com.edvantis.trainee.stringinstrshop.exception.DataValidityException;
import com.edvantis.trainee.stringinstrshop.exception.EmptyDataException;
import com.edvantis.trainee.stringinstrshop.exception.InstrumentNotFoundExeption;
import com.edvantis.trainee.stringinstrshop.exception.WrongIndexException;
import com.edvantis.trainee.stringinstrshop.instruments.Guitar;
import com.edvantis.trainee.stringinstrshop.instruments.StringInstrument;
import com.edvantis.trainee.stringinstrshop.person.Client;
import com.edvantis.trainee.stringinstrshop.person.Person;
import com.edvantis.trainee.stringinstrshop.person.Worker;
import com.edvantis.trainee.stringinstrshop.storage.MusicInstrumentsStorage;

/**
 * @author ����
 *
 */
public class OrderTest {

	Order order;
	Person client;
	Person worker;
	MusicInstrumentsStorage storage;
	ArrayList<StringInstrument> list;

	@Before
	public void init() throws EmptyDataException, DataValidityException {
		client = new Client(1, "N", "Iv", "naz@gmail.com", "12345678");
		worker = new Worker(1, "N", "Iv", "naz@gmail.com", "12345678", Occupation.WORKER);
		storage = MusicInstrumentsStorage.getInstance();
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.order.Order#setClient(com.edvantis.trainee.stringinstrshop.person.Person)}
	 * .
	 * 
	 * @throws EmptyDataException
	 */
	@Test(expected = EmptyDataException.class)
	public void testSetClient() throws EmptyDataException {
		order = new Order(1, null, worker);
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.order.Order#setWorker(com.edvantis.trainee.stringinstrshop.person.Person)}
	 * .
	 * 
	 * @throws EmptyDataException
	 */
	@Test(expected = EmptyDataException.class)
	public void testSetWorker() throws EmptyDataException {
		order = new Order(1, client, null);
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.order.Order#addItem(com.edvantis.trainee.stringinstrshop.instruments.StringInstrument)}
	 * 
	 * @throws EmptyDataException
	 * @throws DataIntegrityException
	 * @throws DataValidityException
	 */
	@Test(expected = EmptyDataException.class)
	public void testAddInstrument() throws EmptyDataException, DataValidityException, DataIntegrityException {
		order = new Order(1, client, worker);
		order.addItem(null);
	}

	/**
	 * 
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 */
	@Test
	public void testAddInstrumentQuantityIncrease() throws EmptyDataException, DataValidityException,
			DataIntegrityException {
		order = new Order(1, client, worker);
		order.addItem(new Guitar(1, "Gibson", "j-35", 75600, 6));
		assertEquals(1, order.getQuantity());
	}

	/**
	 * 
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws WrongIndexException
	 * @throws InstrumentNotFoundExeption
	 */
	@Test(expected = InstrumentNotFoundExeption.class)
	public void testRemoveItemByObject() throws EmptyDataException, DataValidityException, DataIntegrityException,
			WrongIndexException, InstrumentNotFoundExeption {
		order = new Order(1, client, worker);
		order.addItem(new Guitar(1, "Gibson", "j-35", 75600, 6));
		order.removeItem(new Guitar(1, "Gibson", "j-37", 75600, 6));
	}

	/**
	 * test price change
	 * 
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws WrongIndexException
	 * @throws InstrumentNotFoundExeption
	 */
	@Test
	public void testRemoveItemPrice() throws EmptyDataException, DataValidityException, DataIntegrityException,
			WrongIndexException, InstrumentNotFoundExeption {
		order = new Order(1, client, worker);
		order.addItem(new Guitar(1, "Gibson", "j-35", 75600, 6));
		order.removeItem(new Guitar(1, "Gibson", "j-35", 75600, 6));
		assertEquals(0, order.getFullPrice(), 0);
	}

	/**
	 * test quantity change
	 * 
	 * @throws EmptyDataException
	 * @throws DataValidityException
	 * @throws DataIntegrityException
	 * @throws WrongIndexException
	 * @throws InstrumentNotFoundExeption
	 */
	@Test
	public void testRemoveItemQuantity() throws EmptyDataException, DataValidityException, DataIntegrityException,
			WrongIndexException, InstrumentNotFoundExeption {
		order = new Order(1, client, worker);
		order.addItem(new Guitar(1, "Gibson", "j-35", 75600, 6));
		order.removeItem(new Guitar(1, "Gibson", "j-35", 75600, 6));
		assertEquals(0, order.getQuantity(), 0);
	}

}
