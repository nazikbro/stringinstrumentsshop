/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.dao.entities.BodyMaterialDTO;
import com.edvantis.trainee.stringinstrshop.dao.entities.BowDTO;
import com.edvantis.trainee.stringinstrshop.dao.util.HibernateUtil;

/**
 * @author Назік
 *
 */
public class BowDAOImplTest {

	@Ignore
	@Test
	public void testSave() {
		BowDTO bowDTO = new BowDTO();
		BodyMaterialDTO bodyMaterialDTO = new BodyMaterialDTO();
		bodyMaterialDTO.setBodyMaterialId(1);
		bowDTO.setBodyMaterialDTO(bodyMaterialDTO);
		bowDTO.setLength(90);
		HibernateUtil.beginTransaction();
		BowDAOImpl bowDAOImpl = new BowDAOImpl();
		bowDAOImpl.save(bowDTO);
		HibernateUtil.commitTransaction();
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.dao.crud.AbstractDAO#findByID(java.lang.Class, java.lang.Long)}
	 * .
	 */
	@Test
	public void testFindByID() {
		HibernateUtil.beginTransaction();
		BowDAOImpl bowDAOImpl = new BowDAOImpl();
		BowDTO bowDTO = bowDAOImpl.findByID(1);
		HibernateUtil.commitTransaction();
		assertEquals(new Integer(1), bowDTO.getBowId());
	}

}
