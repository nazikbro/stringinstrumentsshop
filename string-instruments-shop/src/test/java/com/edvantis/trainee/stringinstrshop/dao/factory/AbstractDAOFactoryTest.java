/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.factory;

import static org.junit.Assert.*;

import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.dao.crud.BowDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.GenericDAO;
import com.edvantis.trainee.stringinstrshop.dao.crud.impl.BowDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.enums.StringInstrumentDAOType;
import com.edvantis.trainee.stringinstrshop.exception.UnknownTypeException;

/**
 * @author Назік
 *
 */
public class AbstractDAOFactoryTest {

	/**
	 * Test method for {@link com.edvantis.trainee.stringinstrshop.dao.factory.AbstractDAOFactory#getDAO(com.edvantis.trainee.stringinstrshop.dao.enums.StringInstrumentDAOType)}.
	 * @throws UnknownTypeException 
	 */
	@Test
	public void testGetDAO() throws UnknownTypeException {
		AbstractDAOFactory factory = new AbstractDAOFactory(new HibernateDAOFactory());
		assertTrue(factory.getDAO(StringInstrumentDAOType.BOW) instanceof BowDAOImpl);
	}

}
