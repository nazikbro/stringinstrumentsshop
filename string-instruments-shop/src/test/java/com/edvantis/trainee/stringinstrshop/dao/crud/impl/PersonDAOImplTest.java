/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.dao.crud.impl.PersonDAOImpl;
import com.edvantis.trainee.stringinstrshop.dao.entities.PersonDTO;
import com.edvantis.trainee.stringinstrshop.dao.util.HibernateUtil;

/**
 * @author Назік
 *
 */
public class PersonDAOImplTest {

	/**
	 * Test method for
	 * {@link com.edvantis.trainee.stringinstrshop.dao.crud.impl.PersonDAOImpl#findByName(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testFindByName() {
		HibernateUtil.beginTransaction();
		PersonDAOImpl p = new PersonDAOImpl();
		PersonDTO person = p.findByName("nazar", "nazar");
		HibernateUtil.commitTransaction();
		assertEquals("nazar", person.getFirstName());
	}
}
