/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.dao.crud.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.edvantis.trainee.stringinstrshop.dao.entities.BodyMaterialDTO;
import com.edvantis.trainee.stringinstrshop.dao.util.HibernateUtil;

/**
 * @author Назік
 *
 */
public class BodyMaterialDAOImplTest {

	/**
	 * 
	 */
	@Test
	public void testFindByName() {
		HibernateUtil.beginTransaction();
		BodyMaterialDAOImpl bodyMaterial = new  BodyMaterialDAOImpl();
		BodyMaterialDTO bodyMaterialDTO = bodyMaterial.findByName("Steel");
		HibernateUtil.commitTransaction();
		assertEquals("Steel", bodyMaterialDTO.getBodyMaterialName());	}

}
