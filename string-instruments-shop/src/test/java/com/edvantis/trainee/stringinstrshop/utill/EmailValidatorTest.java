/**
 * 
 */
package com.edvantis.trainee.stringinstrshop.utill;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * @author ����
 *
 */
@RunWith(Parameterized.class)
public class EmailValidatorTest {

	private String email;
	private Boolean isValid;

	/**
	 * @param email
	 * @param isValid
	 */
	public EmailValidatorTest(String email, boolean isValid) {
		this.email = email;
		this.isValid = isValid;
	}

	@Parameters
	public static Collection<Object[]> email() {
		return Arrays.asList(new Object[][] { { "nazik@yahoo.com", true }, { "nazik-100@yahoo.com", true },
				{ "nazik.100@yahoo.com", true }, { "nazik111@nazik.com", true }, { "nazik-100@nazik.net", true },
				{ "nazik.100@nazik.com.au", true }, { "nazik@1.com", true }, { "nazik@gmail.com.com", true },
				{ "nazik+100@gmail.com", true }, { "nazik-100@yahoo-test.com", true }, { "nazik", false },
				{ "nazik@.com.my", false }, { "nazik123@gmail.a", false }, { "nazik123@.com", false },
				{ "nazik123@.com.com", false }, { ".nazik@nazik.com", false }, { "nazik()*@gmail.com", false },
				{ "nazik@%*.com", false }, { "nazik..2002@gmail.com", false }, { "nazik.@gmail.com", false },
				{ "nazik@nazik@gmail.com", false }, { "nazik@gmail.com.1a", false }, });
	}
	
	@Test
	public void testValidate(){
		assertEquals(isValid,EmailValidator.validate(email));
	}

}
